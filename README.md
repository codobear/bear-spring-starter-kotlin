![Coding Bear logo](coding-bear/logo.png)

# Bear Spring Kotlin Starter

## What is it?
**Bear Spring Kotlin Starter** is a Kotlin-based counterpart to (bear-spring-starter)[https://github.com/codingbearcom/bear-spring-starter], 
a no-nonsense Spring project skeleton that includes all basic libraries and configurations that Coding Bear programmers find useful 
for quick spin-up of a new project.
This project is completely equivalent to `bear-spring-starter`, it "only" differs in the language. The Kotlin version also lacks Checkstyle support, 
as Checkstyle itself does work with Kotlin sources files. Currently, no Kotlin-specific replacement is configured, but we'd like to integrate 
some linter later, maybe Klint or Detect. 

## Why are we sharing this?
Setting up new project can be a lengthy and painful process with lot of trial and error, 
that can take from several hours up to several working days, depending on your experience-level and on how often you face 
such a situation. 

We at Coding Bear felt the need for a go-to skeleton, that we could use each time we start working on something new and 
that would save us from doing repetitive work. With our starter skeleton, we can (almost) skip the set-up phase, 
spend our time in a more efficient manner and save our clients some money.

And because we love, use and benefit from open source, now when we have this opportunity, it feels like a right time to give 
something back.

## How should you use it?
At your own risk :-) Browse the code, adjust it to your taste, build something useful on top of it.

Just a little disclaimer - we are mainly Java guys and at this moment we are only playing with Kotlin and our Kotlin code might not 
really idiomatic. Unlike our Java-based starter, this skeleton has not been tried out on a real serious project and it strongly adheres 
to libraries and technologies from the Java world (namely Spring), although we know there are some Kotlin-specific options out there. 
Those might or might not be a better fit for you, but we decided to create one-to-one Kotlin version of our Java project starter.
Take it as it is, you have been warned :-) 

## Is it under active development?
Depends, rather occasionally. We want to keep the skeleton reasonably up to date (i.e. not necessarily always the latest version of all the libraries, 
but the ones we are currently using). We also plan to add more features later on, but at the same time, the project should not get overly complex.
We described this as a "no-nonsense" skeleton and we'd like to keep it as such.   
Thus, we might create a feature-richer version in a separate repository in the future or create a separate branch.

## Are there some known issues?
Yes. For some reason, `./gradlew bootRun` does not work properly (while `bootJar` works and the resulting executable jar works as well).
We will take a deeper look at it once we have some spare time. Or maybe it is a nice opportunity for you (see next paragraph) :-). 

## Can I help?
Definitely, feel free to open issues and pull requests. Don't hesitate to comment, fork, contact us and give feedback.    

# Quick feature overview
The tech stack is recent while still being slightly conservative and time-proven. It reflects how we think main-stream modern 
applications are (or should be) built today.

## Technical features
* Kotlin, Gradle wrapper
* Spring Boot, Spring dependency injection
* Spring Data, JPA, PostgreSQL
* REST API (JAX-RS / RestEasy)
* JUnit5, integration tests using Docker, Mockito
* custom-built .gitignore
* sample Sonar integration

## Sample use case
The project also contains one sample end-to-end scenario. 

* fully functional sample "user" REST endpoint, service and repository
* prepared connection to PostgreSQL database through Spring JPA
* sample unit test and integration test

# Usage

## Development database

Project includes Gradle plugin that allows simple spin-ups of docker-compose containing PostgreSQL instance by running Gradle task.

To change container settings, simply edit YAML config file in `/config/docker/environment-compose.yml`. These changes should then be propagated to Hibernate configuration in all applications depending on this database (namely `application.yml` config files in resource folder of each runnable project).

All configuration happens in `/config/docker/environment-compose.yml` file. It obeys [standard compose file structure](https://docs.docker.com/compose/compose-file/).

Hook up of Gradle plugin happens in `/build.gradle.kts` script.

## Running the project

### Build
* To perform only common build, run `./gradlew clean build`
* To build and run tests, run `./gradlew clean build integrationTests`
* To create an executable jar file `./gradlew bootJar` (it should be created in `/application/build/libs`)

### Develpoment PostgreSQL database

```bash
./gradlew composeUp
```

This will start a PostgreSQL DB instance running in the docker container and applies the `application/src/main/resources/sql/schema.sql` script.

You can use e.g. `psql -h localhost -U postgres -d bss` to connect to the DB and browse the structure.

### Sample project

Sample project depends on having database running, so remember to spin it up (project includes option to run instance in Docker container included as Gradle task in root build script - see [Running PostgreSQL database](#postgresql-database)).

Application can be started in multiple ways. 

* use your favourite IDE to run it in a standard or debug mode.
* type `./gradlew bootRun` in terminal
* use `java -jar <executable.jar>` (executable jar file has to be created first)

### Sample Sonar integration

Edit `gradle.properties` and set your Sonar instance URI and authentication token, then run `./gradlew sonarqube` and check the result of the 
analysis in your Sonar's interface.

## About Coding Bear

* We are Prague-based software company with passion for doing things better. We love to deliver clean and efficient code.   
* See more of what we do and what we can do for you at our [website](https://codingbear.com).

## Changelog

### v1.1
* Upgraded Gradle to version `7.4.1`
* Removed `build` and `out` folders
* Added `.gitignore`
* Upgraded Postgres to version `14.2-alpine3.15`
 
### List of upgraded dependencies

| Name                                            | Previous version |  New version   |
|-------------------------------------------------|:----------------:|:--------------:|
| org.springframework.boot                        |  2.2.1.RELEASE   |     2.6.5      |
| io.spring.dependency-management                 |  1.0.8.RELEASE   | 1.0.11.RELEASE |
| org.jetbrains.kotlin.jvm                        |      1.3.50      |     1.6.10     |
| org.jetbrains.kotlin.plugin.jpa                 |      1.3.50      |     1.6.10     |
| org.jetbrains.kotlin.plugin.spring              |      1.3.50      |     1.6.10     |
| com.avast.gradle.docker-compose                 |      0.8.12      |     0.13.4     |
| org.sonarqube                                   |       2.8        |      3.3       |
| org.jboss.resteasy                              |   3.8.0.Final    |  3.15.3.Final  |
| org.jboss.resteasy:resteasy-spring-boot-starter |   3.1.0.Final    |  5.0.0.Final   |
| org.postgresql                                  |     42.2.05      |     42.3.3     |
| commons-io:commons-io                           |       2.6        |     2.11.0     |
| org.apache.commons:commons-collections          |      4.4.3       |     4.4.4      |
| org.apache.commons:commons-lang                 |      3.3.9       |     3.12.0     |
| io.rest-assured                                 |      3.1.1       |     4.5.1      |
| com.xebialabs.restito                           |      0.9.3       |     0.9.4      |
| org.junit.jupiter                               |      5.3.2       |     5.8.2      |
