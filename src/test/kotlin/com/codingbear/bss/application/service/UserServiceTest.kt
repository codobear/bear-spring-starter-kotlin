/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.service

import com.codingbear.bss.application.entity.TestUser
import com.codingbear.bss.application.repository.UserRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import java.util.*

internal class UserServiceTest {
    private val userRepository = Mockito.mock(UserRepository::class.java)
    private var userService: UserService? = null

    @BeforeEach
    fun setUp() {
        val user = TestUser()
        user.id = 2
        user.firstName = "John"
        user.lastName = "Doe"
        Mockito.`when`(userRepository.findById(ArgumentMatchers.any())).thenReturn(Optional.of(user))
        userService = UserService(userRepository)
    }

    // just sample test
    @Test
    fun `get user works`() {
        val user = userService!!.getUser(2)
        Assertions.assertEquals(Integer.valueOf(2), user.id)
        Assertions.assertEquals("John Doe", user.name)
    }
}
