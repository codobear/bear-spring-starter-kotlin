/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.service.resource

import com.codingbear.bss.application.service.DbClearingExtension
import com.codingbear.bss.application.service.JsonUtils.Companion.assertEqualsJson
import io.restassured.RestAssured
import org.apache.commons.io.IOUtils
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.net.URI
import java.nio.charset.StandardCharsets

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(scripts = ["/sql/test-data.sql"])
@ExtendWith(SpringExtension::class, DbClearingExtension::class)
@DirtiesContext
internal class UserResourceTest {
    @LocalServerPort
    private val port = 0
    private var resourceUrl: String? = null
    @BeforeEach
    fun setUp() {
        resourceUrl = URI.create(String.format("http://localhost:%d/v1/user", port)).toString()
    }

    @Test
    fun `get single user should return expected json`() {
        val response = RestAssured.given()
                .`when`()["$resourceUrl/1"]
                .then()
                .statusCode(200)
                .extract()
                .asString()
        val expected = IOUtils.toString(this.javaClass.getResourceAsStream("/resource/user-resource/user-response.json"), StandardCharsets.UTF_8)
        assertEqualsJson(expected, response)
    }
}
