/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.service

import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension

/**
 * A JUnit extension that clears database data after each test method.
 */
class DbClearingExtension : AfterEachCallback {

    @Override
    override fun afterEach(context: ExtensionContext) {
        clearDatabase(context)
    }

    private fun clearDatabase(context: ExtensionContext) {
        val applicationContext = SpringExtension.getApplicationContext(context)
        val jdbcTemplate = applicationContext.getBean(JdbcTemplate::class.java)

        val tables = jdbcTemplate.query(TABLES_QUERY) { r, _ -> r.getString(1) }

        val queryBuilder = StringBuilder()
        for (table in tables) {
            queryBuilder.append("TRUNCATE ")
            queryBuilder.append(table)
            queryBuilder.append(" CASCADE; ")
        }

        jdbcTemplate.execute(queryBuilder.toString())
    }

    companion object {
        private const val TABLES_QUERY =
                """
                    SELECT input_table_name AS truncate_query 
                    FROM(
                      SELECT table_schema || '.' || table_name AS input_table_name 
                      FROM information_schema.tables 
                      WHERE 
                        table_schema NOT IN ('pg_catalog', 'information_schema') 
                        AND table_schema NOT LIKE 'pg_toast%'
                    ) AS information;
                """
    }
}
