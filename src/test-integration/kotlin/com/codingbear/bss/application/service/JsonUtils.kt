/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.service

import org.json.JSONException
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import org.junit.jupiter.api.Assertions.fail

class JsonUtils {
    companion object {
        /**
         * Asserts equality of two JSON objects given as String. Indentation, whitespaces and order of elements is ignored.
         *
         * @param expected Expected JSON string
         * @param actual   Actual JSON string
         */
        fun assertEqualsJson(expected: String, actual: String) {
            try {
                JSONAssert.assertEquals(expected, actual, JSONCompareMode.NON_EXTENSIBLE)

            } catch (e: AssertionError) {
                fail<Any>("Expected: $expected\nBut Found: $actual")
            } catch (e: JSONException) {
                fail<Any>("Expected: $expected\nBut Found: $actual")
            }

        }
    }
}
