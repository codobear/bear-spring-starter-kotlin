/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.resource

import com.codingbear.bss.application.resource.entity.UserResponse
import com.codingbear.bss.application.service.UserService
import org.springframework.stereotype.Component

import javax.inject.Inject
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

/**
 * The implementation of the User REST endpoints.
 */
@Component
@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class UserResource @Inject
constructor(private val userService: UserService) {

    /**
     * Returns information about specified user.
     *
     * @param userId id of user.
     * @return information about user.
     */
    @GET
    @Path("{userId}")
    fun getUser(@PathParam("userId") userId: Int): UserResponse = userService.getUser(userId)
}
