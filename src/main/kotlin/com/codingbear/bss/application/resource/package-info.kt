/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */

/**
 * Package containing REST resources.
 */
package com.codingbear.bss.application.resource
