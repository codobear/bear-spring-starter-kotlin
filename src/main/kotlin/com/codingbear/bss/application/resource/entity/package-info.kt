/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */

/**
 * Package containing entity for REST resources.
 */
package com.codingbear.bss.application.resource.entity
