/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.resource.entity

/**
 * REST entity representing user data.
 */
data class UserResponse (var id: Int? = null, var name: String? = null)

