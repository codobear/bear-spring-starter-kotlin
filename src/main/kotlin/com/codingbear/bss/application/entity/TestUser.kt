/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Represents data for user.
 */
@Entity
class TestUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null

    var firstName: String? = null

    var lastName: String? = null

    override fun toString(): String {
        return ("User{id=$id, firstName='$firstName', lastName='$lastName'}")
    }
}
