/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.stereotype.Service

import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider

import com.fasterxml.jackson.core.JsonParser.Feature.AUTO_CLOSE_SOURCE
import com.fasterxml.jackson.databind.DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE

/**
 * Configuration of Jackson provider used by JAX-RS client.
 */
@Provider
@Service
class JacksonConfig : ContextResolver<ObjectMapper> {

    private val mapper: ObjectMapper = ObjectMapper()

    init {
        mapper.registerModule(JavaTimeModule())
        mapper.disable(ADJUST_DATES_TO_CONTEXT_TIME_ZONE)
        mapper.enable(AUTO_CLOSE_SOURCE)
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL) // skip null fields in the serialized JSON
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    @Override
    override fun getContext(arg0: Class<*>): ObjectMapper = mapper
}
