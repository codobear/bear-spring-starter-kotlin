/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * Main application class.
 */
@SpringBootApplication
open class Application

/**
 * Main entry point.
 *
 * @param args - application run arguments
 */
fun main(args: Array<String>) {
    runApplication<Application>(*args)
}

