/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.filter

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.util.AntPathMatcher
import java.io.*
import java.net.URI
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.stream.Collectors
import javax.annotation.PostConstruct
import javax.annotation.Priority
import javax.ws.rs.ConstrainedTo
import javax.ws.rs.RuntimeType
import javax.ws.rs.container.ContainerRequestContext
import javax.ws.rs.container.ContainerRequestFilter
import javax.ws.rs.container.ContainerResponseContext
import javax.ws.rs.container.ContainerResponseFilter
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.ext.Provider
import javax.ws.rs.ext.WriterInterceptor
import javax.ws.rs.ext.WriterInterceptorContext
import kotlin.math.min
import kotlin.streams.toList

/**
 * A filter that logs requests received and responses sent by the server.
 */
@ConstrainedTo(RuntimeType.SERVER)
@Priority(Int.MIN_VALUE)
@Component
@Provider
class ServerLoggingFilter
/**
 * Constructor.
 *
 * @param config logging filter configuration.
 */ @Autowired constructor(private val config: Config) : ContainerRequestFilter, ContainerResponseFilter, WriterInterceptor {
    companion object {
        private const val REQUEST_MDC_KEY = "request-id"
        private const val ENTITY_LOGGER_PROPERTY = "ENTITY_LOGGER_PROPERTY"
        private const val RESPONSE_LOGGED_PROPERTY = "RESPONSE_LOGGED_PROPERTY"
        private val UTF8 = StandardCharsets.UTF_8
        /**
         * Prefix will be printed before requests.
         */
        private const val REQUEST_PREFIX = "> "
        /**
         * Prefix will be printed before response.
         */
        private const val RESPONSE_PREFIX = "< "
        private const val NOTIFICATION_PREFIX = "* "
        private const val SECRET_TEXT = "-- secret --"
        private val COMPARATOR = Comparator { o1: Map.Entry<String, List<String?>>, o2: Map.Entry<String, List<String?>> -> o1.key.compareTo(o2.key, ignoreCase = true) }
        private val TEXT_MEDIA_TYPE = MediaType("text", "*")
        private val READABLE_APP_MEDIA_TYPES: MutableSet<MediaType> = HashSet()
        private const val REQUEST_NOTE = "Server has received a request"
        private const val RESPONSE_NOTE = "Server responded with a response"

        init {
            READABLE_APP_MEDIA_TYPES.add(TEXT_MEDIA_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_ATOM_XML_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_FORM_URLENCODED_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_JSON_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_SVG_XML_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_XHTML_XML_TYPE)
            READABLE_APP_MEDIA_TYPES.add(MediaType.APPLICATION_XML_TYPE)
        }

        private fun doLogMessage(loggedMessage: StringBuilder, buffer: ByteArrayOutputStream, maxEntitySize: Int, charset: Charset): String { // write entity to the builder
            val entity = buffer.toByteArray()
            loggedMessage.append(String(entity, 0, min(entity.size, maxEntitySize), charset))
            if (entity.size > maxEntitySize) {
                loggedMessage.append("...more...")
            }
            loggedMessage.append('\n')
            return loggedMessage.toString()
        }

    }

    private val log = LoggerFactory.getLogger(ServerLoggingFilter::class.java)
    private val antPathMatcher = AntPathMatcher()
    override fun filter(requestContext: ContainerRequestContext) {
        if (!log.isDebugEnabled || !shouldLog(requestContext)) {
            return
        }
        MDC.put(REQUEST_MDC_KEY, UUID.randomUUID().toString())
        // a flag indicating if the logging is turned on for this exchange,
        // so we don't have to evaluate the same conditions in the response
        requestContext.setProperty(RESPONSE_LOGGED_PROPERTY, "")
        val loggedMessage = StringBuilder()
        printRequestLine(loggedMessage, requestContext.method, requestContext.uriInfo.requestUri)
        printPrefixedHeaders(loggedMessage, REQUEST_PREFIX, requestContext.headers)
        if (config.logBody && hasEntity(requestContext) && isReadable(requestContext.mediaType)) {
            val charset = getCharset(requestContext.mediaType)
            val contentLength = getContentLength(requestContext)
            requestContext.entityStream = IncomingLoggingStream(loggedMessage, requestContext.entityStream, config, charset, log, contentLength!!)
        } else if (log.isDebugEnabled) {
            log.debug(loggedMessage.toString())
        }
    }

    override fun filter(requestContext: ContainerRequestContext, responseContext: ContainerResponseContext) {
        if (requestContext.getProperty(RESPONSE_LOGGED_PROPERTY) == null) {
            return
        }
        val loggedMessage = StringBuilder()
        printResponseLine(loggedMessage, responseContext.status)
        printPrefixedHeaders(loggedMessage, RESPONSE_PREFIX, responseContext.stringHeaders)
        if (config.logBody && responseContext.hasEntity() && isReadable(responseContext.mediaType)) {
            val charset = getCharset(responseContext.mediaType)
            val stream: OutputStream = OutgoingLoggingStream(loggedMessage,
                    responseContext.entityStream,
                    config,
                    charset,
                    log)
            responseContext.entityStream = stream
            requestContext.setProperty(ENTITY_LOGGER_PROPERTY, stream)
        } else if (log.isDebugEnabled) {
            log.debug(loggedMessage.toString())
        }
        MDC.remove(REQUEST_MDC_KEY)
    }

    private fun shouldLog(context: ContainerRequestContext): Boolean {
        if (config.ignoredUrls == null) {
            return true
        }
        val path = context.uriInfo.getPath(true)
        return config.ignoredUrls!!.stream().noneMatch { pattern: String? -> antPathMatcher.match(pattern!!, path) }
    }

    private fun printRequestLine(loggedMessage: StringBuilder, method: String, uri: URI) {
        loggedMessage.append(NOTIFICATION_PREFIX)
                .append(REQUEST_NOTE)
                .append("\n")
        loggedMessage.append(REQUEST_PREFIX)
                .append(method)
                .append(" ")
                .append(uri.toASCIIString())
                .append("\n")
    }

    private fun printResponseLine(loggedMessage: StringBuilder, status: Int) {
        loggedMessage.append(NOTIFICATION_PREFIX)
                .append(RESPONSE_NOTE)
                .append("\n")
        loggedMessage.append(RESPONSE_PREFIX)
                .append(status)
                .append("\n")
    }

    private fun printPrefixedHeaders(loggedMessage: StringBuilder,
                                     prefix: String,
                                     headers: MultivaluedMap<String, String>) {
        for (headerEntry: Map.Entry<String, List<String?>> in getSortedHeaders(headers.entries)) {
            val value = headerEntry.value
            val header = headerEntry.key

            if (value.size == 1) {
                if (header.equals(HttpHeaders.AUTHORIZATION, ignoreCase = true)) {
                    loggedMessage.append(prefix).append(header).append(": ").append(SECRET_TEXT).append("\n")
                } else {
                    loggedMessage.append(prefix).append(header).append(": ").append(value[0]).append("\n")
                }
            } else {
                val headersList = value.stream()
                        .map { it.toString() }
                        .collect(Collectors.joining(","))
                loggedMessage.append(prefix).append(header).append(": ").append(headersList).append("\n")
            }
        }
    }

    private fun getSortedHeaders(headers: Set<Map.Entry<String, List<String>>>): Set<Map.Entry<String, List<String?>>> {
        val sortedHeaders = TreeSet(COMPARATOR)
        sortedHeaders.addAll(headers)
        return sortedHeaders
    }

    private fun isReadable(mediaType: MediaType?): Boolean {
        if (mediaType != null) {
            for (readableMediaType: MediaType in READABLE_APP_MEDIA_TYPES) {
                if (readableMediaType.isCompatible(mediaType)) {
                    return true
                }
            }
        }
        return false
    }

    // responseContext.hasEntity() cannot be relied upon, because it just check if the response has a content type
    // some clients send a content type header even in requests without body
    private fun hasEntity(requestContext: ContainerRequestContext): Boolean {
        val contentLength = getContentLength(requestContext)
        if (contentLength != null) {
            if (contentLength.compareTo(0) == 0) {
                return false
            }
            if (contentLength > 0) {
                return true
            }
        }
        val values = requestContext.headers["Transfer-Encoding"]
        return values?.stream()?.anyMatch { value: String -> value.equals("Chunked", ignoreCase = true) } ?: false
    }

    private fun getContentLength(requestContext: ContainerRequestContext): Int? {
        val contentLengthStr = requestContext.headers.getFirst("Content-Length")
        var contentLength: Int? = null
        if (contentLengthStr != null) {
            try {
                contentLength = Integer.valueOf(contentLengthStr)
            } catch (e: NumberFormatException) {
                log.warn("Could not parse 'Content-length' header value", e)
            }
        }
        return contentLength
    }

    private fun getCharset(m: MediaType?): Charset {
        val name = m?.parameters?.get(MediaType.CHARSET_PARAMETER)
        return if (name == null) UTF8 else Charset.forName(name)
    }

    @Throws(IOException::class)
    override fun aroundWriteTo(context: WriterInterceptorContext) {
        val loggingStream: OutgoingLoggingStream? = context.getProperty(ENTITY_LOGGER_PROPERTY) as OutgoingLoggingStream
        context.proceed()
        loggingStream?.logMessage()
    }

    private class OutgoingLoggingStream internal constructor(private val loggedMessage: StringBuilder,
                                                             wrappedStream: OutputStream?,
                                                             private val config: Config,
                                                             private val charset: Charset,
                                                             private val log: Logger) : FilterOutputStream(wrappedStream!!) {
        private val buffer = ByteArrayOutputStream()
        fun logMessage() {
            val loggedOutput = doLogMessage(loggedMessage, buffer, config.maxEntitySize, charset)
            if (log.isDebugEnabled) {
                log.debug(loggedOutput)
            }
        }

        @Throws(IOException::class)
        override fun write(i: Int) {
            if (buffer.size() <= config.maxEntitySize) {
                buffer.write(i)
            }
            out.write(i)
        }

        @Throws(IOException::class)
        override fun write(b: ByteArray, off: Int, len: Int) {
            for (i in 0 until len) {
                if (buffer.size() >= config.maxEntitySize) {
                    break
                }
                buffer.write(b[off + i].toInt())
            }
            out.write(b, off, len)
        }

    }

    /**
     * An incoming  entity stream wrapper that stores entity bytes for logging purposes and logs the entity
     * when it has been fully read.
     * The entity has been read when at least one of the following conditions is fulfilled:
     *
     *  * The wrapped input stream returns -1
     *  * A number of bytes that is equal to 'Content-length' header value has been read
     *  * The stream is closed
     *
     */
    private class IncomingLoggingStream internal constructor(private val loggedMessage: StringBuilder,
                                                             wrappedStream: InputStream?,
                                                             private val config: Config,
                                                             private val charset: Charset,
                                                             private val log: Logger,
                                                             contentLength: Int?) : FilterInputStream(wrappedStream) {
        private val buffer = ByteArrayOutputStream()
        private var messageLogged = false
        private var sizeRemaining = 0

        init {
            sizeRemaining = contentLength ?: -1
        }

        @Throws(IOException::class)
        override fun read(): Int {
            val b = `in`.read()
            if (b == -1) {
                logMessage()
                return b
            }
            if (buffer.size() <= config.maxEntitySize) {
                buffer.write(b)
            }
            sizeRemaining--
            if (sizeRemaining == 0) {
                logMessage()
            }
            return b
        }

        @Throws(IOException::class)
        override fun read(b: ByteArray): Int {
            val read = `in`.read(b, 0, b.size)
            if (read == -1) {
                logMessage()
                return read
            }
            for (i in 0 until read) {
                if (buffer.size() >= config.maxEntitySize) {
                    break
                }
                buffer.write(b[i].toInt())
            }
            sizeRemaining -= read
            if (sizeRemaining == 0) {
                logMessage()
            }
            return read
        }

        @Throws(IOException::class)
        override fun read(b: ByteArray, off: Int, len: Int): Int {
            val read = `in`.read(b, 0, b.size)
            if (read == -1) {
                logMessage()
                return read
            }
            for (i in 0 until read) {
                if (buffer.size() >= config.maxEntitySize) {
                    break
                }
                buffer.write(b[off + i].toInt())
            }
            sizeRemaining -= read
            if (sizeRemaining == 0) {
                logMessage()
            }
            return read
        }

        @Throws(IOException::class)
        override fun close() {
            logMessage()
            `in`.close()
        }

        private fun logMessage() {
            if (messageLogged) {
                return
            }
            messageLogged = true
            val loggedOutput = doLogMessage(loggedMessage, buffer, config.maxEntitySize, charset)
            if (log.isDebugEnabled) {
                log.debug(loggedOutput)
            }
        }
    }

    /**
     * Logging filter configuration.
     */
    @Component
    @ConfigurationProperties(prefix = "server-logging")
    @EnableConfigurationProperties
    class Config {
        var maxEntitySize = 1000
        var logBody = true
        /**
         * A list of URLs in Ant-style path patterns (see [org.springframework.util.AntPathMatcher]).
         * Request and responses whose URL matches one of the patters, will not be logged.
         */
        var ignoredUrls: List<String>? = null

        /**
         * Validates configuration parsed from config file.
         */
        @PostConstruct
        fun validate() {
            if (ignoredUrls != null) { // the patterns will be matched with URIs starting with '/, so they must start with '/', too
                ignoredUrls = ignoredUrls!!.stream()
                        .map { if (it.startsWith("/")) it else "/$it" }
                        .toList()
            }
        }
    }
}
