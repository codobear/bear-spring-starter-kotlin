/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */

/**
 * Package representing the main application.
 */
package com.codingbear.bss.application
