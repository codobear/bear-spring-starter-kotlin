/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */

/**
 * Contains database repositories in the sense of [org.springframework.data.repository.Repository].
 */
package com.codingbear.bss.application.repository
