/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */

/**
 * Package containing application's business logic.
 */
package com.codingbear.bss.application.service
