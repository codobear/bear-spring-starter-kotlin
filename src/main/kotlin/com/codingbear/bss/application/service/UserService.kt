/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
package com.codingbear.bss.application.service

import com.codingbear.bss.application.entity.TestUser
import com.codingbear.bss.application.repository.UserRepository
import com.codingbear.bss.application.resource.entity.UserResponse
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import javax.inject.Inject
import javax.ws.rs.NotFoundException

/**
 * Logic for working with users.
 */
@Component
open class UserService @Inject
constructor(private val userRepository: UserRepository) {

    /**
     * Find user by given id.
     *
     * @param userId id of user.
     * @return user information.
     * @throws NotFoundException when specified user id not exist.
     */
    @Transactional(readOnly = true)
    open fun getUser(userId: Int): UserResponse {
        return userRepository.findById(userId).map { u -> toUserResponse(u) }.orElseThrow { NotFoundException() }
    }

    private fun toUserResponse(user: TestUser): UserResponse = UserResponse(user.id, "${user.firstName} ${user.lastName}")
}
