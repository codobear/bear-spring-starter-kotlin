import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.bundling.BootJar

/*
 * Copyright (c) 2022 Coding Bear s.r.o.
 */
plugins {
    id("org.springframework.boot") version "2.6.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.6.20"
    kotlin("plugin.jpa") version "1.6.20"
    kotlin("plugin.spring") version "1.6.20"

    id("com.avast.gradle.docker-compose") version "0.14.6"
    id("org.sonarqube") version "3.3"
    id("project-report")
}

group = "com.codingbear"
version = "1.1"
java.sourceCompatibility = JavaVersion.VERSION_11

tasks.test {
    useJUnitPlatform()

    testLogging {
        showStandardStreams = true
    }
}

sourceSets {
    create("integrationTests") {
        java.srcDirs(  "src/test-integration/kotlin")
        resources.srcDir("src/test-integration/resources")
        compileClasspath += sourceSets.main.get().output
        runtimeClasspath += sourceSets.main.get().output
    }
}

val integrationTestsImplementation: Configuration by configurations.getting {
    extendsFrom(configurations["testImplementation"])
}

val integrationTestsRuntimeOnly: Configuration by configurations.getting {
    extendsFrom(configurations["testRuntimeOnly"])
}

val integrationTests = task<Test>("integrationTests") {
    description = "Runs integration tests."
    group = "verification"
    testClassesDirs = sourceSets["integrationTests"].output.classesDirs
    classpath = sourceSets["integrationTests"].runtimeClasspath

    shouldRunAfter("test")
    useJUnitPlatform()

    testLogging {
        showStandardStreams = true
    }

    dependsOn(tasks["composeUp"])
    mustRunAfter(tasks["composeUp"])

    finalizedBy(tasks["composeDown"])
}

tasks.compileTestJava {
    // run integration test compilation in build action
    finalizedBy(tasks["compileIntegrationTestsJava"])
}

configurations {
    implementation {
        exclude("org.springframework.boot", "spring-boot-starter-tomcat")
        exclude("org.springframework", "spring-webmvc")
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web") {
        exclude("org.springframework.boot:spring-boot-starter-tomcat")
        exclude("org.springframework:spring-webmvc")
    }

    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("org.springframework.boot:spring-boot-starter-web-services")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-jetty")
    implementation("org.springframework.boot:spring-boot-starter-logging")
    implementation("javax.inject:javax.inject:1")

    implementation("org.jboss.resteasy:resteasy-spring-boot-starter:5.0.0.Final")
    implementation("org.postgresql:postgresql:42.3.3")

    implementation("commons-io:commons-io:2.11.0")
    implementation("org.apache.commons:commons-collections4:4.4")
    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("io.rest-assured:rest-assured:4.5.1")
    // since rest-assured 4.0.0+ also xml-path and json-path should be referenced
    implementation("io.rest-assured:xml-path:4.5.1")
    implementation("io.rest-assured:json-path:4.5.1")

    implementation("com.xebialabs.restito:restito:0.9.4") {
        exclude("org.junit.vintage:junit-vintage-engine")
    }

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude("org.junit.vintage:junit-vintage-engine")
    }

    implementation(kotlin("stdlib-jdk8"))

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")

    integrationTestsImplementation("org.springframework.boot", "spring-boot-starter-test")
}

dockerCompose {
    useComposeFiles = listOf("config/docker/environment-compose.yml")
}

tasks.composeDown {
    mustRunAfter(integrationTests)
}

gradle.taskGraph.whenReady {
    tasks.test {
        onlyIf {
            hasTask(integrationTests)
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

// Following statement prevents this error coming from CLI gradlew build:
//   Execution failed for task ':processIntegrationTestsResources'.
//   > Entry resource/user-resource/user-response.json is a duplicate but no duplicate handling strategy has been set.
//     Please refer to
//     https://docs.gradle.org/7.4.1/dsl/org.gradle.api.tasks.Copy.html#org.gradle.api.tasks.Copy:duplicatesStrategy
//     for details.
tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE;
}

// by default the Spring boot plugin will replace a normal jar with boot jar
// this is a problem if there is another module that depends on this one, because boot jar
// cannot be used as a dependency
tasks.getByName<Jar>("jar") {
    enabled = true
}

tasks.getByName<BootJar>("bootJar") {
    archiveClassifier.set("executable")
    archiveVersion.set("")
    launchScript()
}

repositories {
    mavenCentral()
}
